package org.oscarehr.integration.ebs.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import ca.ontario.health.ebs.EbsFault;
import ca.ontario.health.edt.CommonResult;
import ca.ontario.health.edt.Detail;
import ca.ontario.health.edt.DetailData;
import ca.ontario.health.edt.DownloadData;
import ca.ontario.health.edt.DownloadResult;
import ca.ontario.health.edt.EDTDelegate;
import ca.ontario.health.edt.ResourceResult;
import ca.ontario.health.edt.ResourceStatus;
import ca.ontario.health.edt.ResponseResult;
import ca.ontario.health.edt.TypeListData;
import ca.ontario.health.edt.TypeListResult;
import ca.ontario.health.edt.UploadData;
import ca.ontario.health.hcv.Faultexception;
import ca.ontario.health.hcv.HCValidation;
import ca.ontario.health.hcv.HcvRequest;
import ca.ontario.health.hcv.HcvResults;
import ca.ontario.health.hcv.Person;
import ca.ontario.health.hcv.Requests;

public class ElectronicDataTransferClientTest {
	
	private static Logger logger = Logger.getLogger(ElectronicDataTransferClientTest.class);
	
	private static final String EDT_SERVICE_URL = "https://ws.conf.ebs.health.gov.on.ca:1441/EDTService/EDTService";
	private static final String HCV_SERVICE_URL = "https://ws.conf.ebs.health.gov.on.ca:1440/HCVService/HCValidationService";

	private static final String MCEDT_CLAIMS_FILE = "HEBV03J201203310005      BGAA17033200                                          \r\n" + 
			"HEH8030482940NN19600527 EDT2P1 HCPP      110020110907     3821                 \r\n" + 
			"HETK028A  0062750120110907616                                                  \r\n" + 
			"HETC003A  0077200120110907780                                                  \r\n" + 
			"HEH5504423178  19811020 EDT2P2 HCPP      110020110907     3821                 \r\n" + 
			"HETA007A  0032350120110907706                                                  \r\n" + 
			"HETC003A  0077200120110907780                                                  \r\n" + 
			"HEH3779764905  19360527 EDT2P3 HCPP      110020110907     3821                 \r\n" + 
			"HETK028A  0062750120110907099                                                  \r\n" + 
			"HETC003A  0077200120110907780                                                  \r\n" + 
			"HEH1936104668DG19490416 EDT2P4 HCPP      110020110909     3821                 \r\n" + 
			"HETE430A  0011150120110909709                                                  \r\n" + 
			"HETA007A  0034700120110909709                                                  \r\n" + 
			"HETC003A  0077200120110909780                                                  \r\n" + 
			"HETK028A  0062750120110909709                                                  \r\n" + 
			"HETG394A  0006750120110909709                                                  \r\n" + 
			"HEH1880870926AL20041116 EDT2P5 HCPP      110020110921     3821                 \r\n" + 
			"HETA007A  0034700120110921917                                                  \r\n" + 
			"HETK028A  0062750120110921917                                                  \r\n" + 
			"HETE430A  0011150120110921917                                                  \r\n" + 
			"HETC003A  0077200120110921917                                                  \r\n" + 
			"HETG394A  0006750120110921917                                                  \r\n" + 
			"HEH1839882220  19931112 EDT2P6 HCPP                       3821                 \r\n" + 
			"HETG394A  0006750120120205304                                                  \r\n" + 
			"HETA007A  0034700120120205304                                                  \r\n" + 
			"HEH1529932244FH19970806 EDT2P7 HCPP      110020111005     3821                 \r\n" + 
			"HETC003A  0077200120120105300                                                  \r\n" + 
			"HETA007A  0034700120120105300                                                  \r\n" + 
			"HETG394A  0006750120120105300                                                  \r\n" + 
			"HEH1600719627JA19301209 EDT2P8 HCPP      110020111005     3821                 \r\n" + 
			"HETA007A  0034700120120205616                                                  \r\n" + 
			"HETE430A  0011550120120205616                                                  \r\n" + 
			"HETK028A  0062750120120205616                                                  \r\n" + 
			"HETG394A  0006750120120205616                                                  \r\n" + 
			"HETC003A  0077200120110205616                                                  \r\n" + 
			"HEH16832889871219300813 EDT2P9 HCPP                       3821                 \r\n" + 
			"HETA007A  0034700120120206628                                                  \r\n" + 
			"HEE0009000000027                                                               ";
	
	private EDTDelegate delegate; 
	
	@Test
	@Ignore
	public void testValidateHealthCard() {
		EdtClientBuilder client = newBuilder(HCV_SERVICE_URL, false);

		HCValidation validation = null;
		try {
			validation = client.build(HCValidation.class);
		} catch (Exception e) {
			e.printStackTrace();

			fail("Unable to initialize");
		}
		
		Requests requests = new Requests();
		HcvRequest request = new HcvRequest();
		request.setHealthNumber("8222783261"); // 10-digit code
		request.setVersionCode("ON"); // 2 letter code
		requests.getHcvRequest().add(request);
		
		request = new HcvRequest();
		request.setHealthNumber("9510351662");
		request.setVersionCode("JM");
		requests.getHcvRequest().add(request);
		
		request = new HcvRequest();
		request.setHealthNumber("1111111111");
		request.setVersionCode("ON");
		requests.getHcvRequest().add(request);
		
		HcvResults results = null;
		try {
			results = validation.validate(requests, "en");
		} catch (Faultexception e) {
			EbsFault fault = e.getFaultInfo();
			logger.error(fault.getCode() + " " + fault.getMessage());
			fail();
		} catch (Exception e) {
			e.printStackTrace();
			
			fail();
		}
		
		assertNotNull(results);
		assertNotNull(results.getResults());
		
		for(Person person : results.getResults()) {
			assertNotNull(person);
			
			assertNotNull(person.getResponseAction());
			assertNotNull(person.getResponseCode());
			assertNotNull(person.getResponseID());
			assertNotNull(person.getFirstName());
			assertNotNull(person.getLastName());
		}
		
	}

	private EdtClientBuilder newBuilder(String serviceUrl) {
		return newBuilder(serviceUrl, true);
	}
	
	private EdtClientBuilder newBuilder(String serviceUrl, boolean useSpecificCredentials) {
		EdtClientBuilderConfig config = new EdtClientBuilderConfig();
		config.setLoggingRequired(true);
		config.setKeystoreUser("alias");
		config.setKeystorePassword("pass");
		if (useSpecificCredentials) {
			config.setUserNameTokenUser("confsu61@outlook.com");
			config.setUserNameTokenPassword("sU3p*Mks5ZBr");
			config.setServiceUrl(serviceUrl);
			config.setConformanceKey("24a1ad39-255a-4693-ae58-dedabd2c07a0");
			config.setServiceId("010637");
		} else {
			config.setUserNameTokenUser("confsu61@outlook.com");
			config.setUserNameTokenPassword("Password0!");
			config.setServiceUrl(serviceUrl);
			config.setConformanceKey("844b6fcf-07e1-4b30-963d-d15b30a61bad");
			config.setServiceId("010637");
		}
		
		EdtClientBuilder clientBuilder = new EdtClientBuilder(config);
		return clientBuilder;
	}
	
	/**
	 * DOWNLOAD OBEC Response OO
	 * DOWNLOAD General Communications GCM
	 * UPLOAD Stale Dated Claim file SDC
	 * UPLOAD OBEC Mail File Reject Message (from inbound) OR
	 * UPLOAD Error Reports ER
	 * DOWNLOAD OBEC Mail File Reject Message (from inbound) OR
	 * DOWNLOAD Claims Mail File Reject Message (from inbound) MR
	 * UPLOAD Remittance Advice Extract RS
	 * UPLOAD Claims Mail File Reject Message (from inbound) MR
	 * UPLOAD Error Reports Extract ES
	 * UPLOAD Remittance Advice RA
	 * DOWNLOAD Stale Dated Claim file SDC
	 * UPLOAD OBEC Response OO
	 * DOWNLOAD Error Reports Extract ES
	 * UPLOAD OBEC inbound file OB
	 * DOWNLOAD Remittance Advice Extract RS
	 * DOWNLOAD Claim File CL
	 * UPLOAD Batch Edit BE
	 * DOWNLOAD Remittance Advice RA
	 * UPLOAD General Communications GCM
	 * UPLOAD Claim File CL
	 * DOWNLOAD OBEC inbound file OB
	 * DOWNLOAD Batch Edit BE
	 * DOWNLOAD Error Reports ER
	 */
	@Test
	public void testUpload() {
		List<UploadData> uploadDataList = new ArrayList<UploadData>();
		UploadData uploadData = new UploadData();
		uploadData.setContent(MCEDT_CLAIMS_FILE.getBytes());
		uploadData.setDescription("Test Claims File");
		uploadData.setResourceType("CL");
		uploadDataList.add(uploadData);
		
		ResourceResult resourceResult = null;
		try {
			resourceResult = delegate.upload(uploadDataList);
		} catch (ca.ontario.health.edt.Faultexception e) {
			failOnFault(e);
		}
		
		List<ResponseResult> responseResultList = resourceResult.getResponse();
		assertNotNull(responseResultList);
		assertFalse(responseResultList.isEmpty());
		
		for(ResponseResult r : responseResultList) {
			String description = r.getDescription();
		    BigInteger resourceID = r.getResourceID();
		    CommonResult result = r.getResult();
		    ResourceStatus status = r.getStatus();
		    
		    logger.info("" + resourceID + " " + description + " " + result + " " + status);
		}
	}
	
	@Test
	public void testDownload() throws Exception {

		Detail detail = null;
		int page = 1;		
		List<BigInteger> downloadList = new ArrayList<BigInteger>();

		while((detail = delegate.list( null, null, BigInteger.valueOf(page))) != null) {
			
			if(detail.getResultSize().intValue() == 0) {
				break;
			}
			
			for(DetailData dd : detail.getData()) {
				logger.debug("resource found = " +dd.getResourceID()  + "," + dd.getResourceType() + "," + dd.getStatus() + "," + dd.getDescription());
				downloadList.add(dd.getResourceID());
			}
			page++;
		}
		
		//we could download them all, but lets stick with 2
		int size = Math.min(2, downloadList.size());
		downloadList = downloadList.subList(0, size);
		
		newEdtClient();
		
		DownloadResult download = delegate.download(downloadList);
		assertNotNull(download);
		assertEquals("Number of downloads doesn't match the expected size", size, download.getData().size());
		
		for(DownloadData data : download.getData()) {
			byte[] content = data.getContent();
			assertNotNull(content);
			assertTrue(content.length > 0);
		}
		
	}
	
	public void testInfo() throws Exception {
		List<BigInteger> resourceIds = new ArrayList<BigInteger>();
		delegate.info(resourceIds);
	}
	
	@Test
	public void testList() {
		Detail detailList = null;
		try {
			detailList = delegate.list(null, null, null);
		} catch (ca.ontario.health.edt.Faultexception e) {
			failOnFault(e);
		}
		List<DetailData> detailData = detailList.getData();
		
		assertNotNull(detailData);
		assertFalse(detailData.isEmpty());
		
		for(DetailData d : detailData) {
			String description = d.getDescription();
		    String resourceType = d.getResourceType();
		    XMLGregorianCalendar modifyTimestamp = d.getModifyTimestamp();
		    BigInteger resourceID = d.getResourceID();
		    CommonResult result = d.getResult();
		    ResourceStatus status = d.getStatus();
		    
			logger.info(description + " " + resourceType + " " + resourceID
					+ " " + result + " " + status + " " + modifyTimestamp);
		}
	}
	
	@Test
	public void testGetTypeList() {
		TypeListResult result = null;
		try {
			result = delegate.getTypeList();
		} catch (ca.ontario.health.edt.Faultexception e) {
			failOnFault(e);
		}

		assertNotNull(result);
		assertFalse("Expected result to contain resource data", result
				.getData().isEmpty());

		SortedSet<String> resourceTypes = new TreeSet<String>();
		for (TypeListData data : result.getData()) {
			assertNotNull(data);
			logger.info(data.getAccess() + " " + data.getDescriptionEn().trim() + " " + data.getResourceType());
			resourceTypes.add(data.getResourceType());
		}
		
		logger.info("Available resource types: " + resourceTypes);
	}

	private void failOnFault(ca.ontario.health.edt.Faultexception e) {
		EbsFault fault = e.getFaultInfo();
		logger.error("Fault: " + fault.getCode() + " " + fault.getMessage());
		fail();
	}

	@Before
	public void newEdtClient() {
		EdtClientBuilder client = newBuilder(EDT_SERVICE_URL);

		EDTDelegate delegate = null;
		try {
			delegate = client.build(EDTDelegate.class);
		} catch (Exception e) {
			logger.error("Unable to initialize", e);
			fail();
		}
		this.delegate = delegate;
	}
}
