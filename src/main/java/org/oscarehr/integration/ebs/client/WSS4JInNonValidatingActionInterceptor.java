package org.oscarehr.integration.ebs.client;

import java.util.List;
import java.util.Map;

import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.ws.security.WSSecurityEngineResult;

/**
 * Overrides CXF action processing to overcome limitation of non-symmetric
 * service call encryptions.
 * 
 * <p/>
 * 
 * By default CXF requires identical actions to be configured on In and Out
 * interceptors. Otherwise, it triggers #{@link org.apache.ws.security.WSSecurityException}. 
 * In MCEDT case, actions can not be identical, as there is SOAP request must not be encrypted, 
 * but SOAP response comes back in an encrypted form. To allow for that,
 * {@link #checkReceiverResultsAnyOrder(List, List)} is overridden to accept all
 * results.
 */
public class WSS4JInNonValidatingActionInterceptor extends WSS4JInInterceptor {

	public WSS4JInNonValidatingActionInterceptor() {
		super();
	}

	public WSS4JInNonValidatingActionInterceptor(boolean ignore) {
		super(ignore);
	}

	public WSS4JInNonValidatingActionInterceptor(Map<String, Object> properties) {
		super(properties);
	}
	
	/**
	 * Overrides parent method to allow all results.
	 * 
	 * @return
	 * 		Returns true for all parameters.
	 * 
	 * @see org.apache.ws.security.handler.WSHandler#checkReceiverResultsAnyOrder(java.util.List,
	 *      java.util.List)
	 */
	@Override
	protected boolean checkReceiverResultsAnyOrder(
			List<WSSecurityEngineResult> wsResult, List<Integer> actions) {
		return true;
	}

}
